<div class="modal fade" id="modal-book">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="modal-title">Réservation</h2>
			</div>
			<div class="modal-body">
				<p id="modal-text"></p>
				<div id="modal-booking">
					<h3>S'inscrire...</h3>
					<button type="button" class="btn btn-default" id="modal-input-self">Cliquez ici pour s'inscrire</button>
					<hr class="line">
					<h3>Inscire quelqu'un...</h3>
					<div class="input-group">
						<input type="text" class="form-control" id="modal-input" placeholder="Entrez un nom..."/>
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" id="modal-input-else">&nbsp;</button>
						</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
				<button type="button" class="btn btn-primary" id="modal-validate" data-dismiss="modal">Valider</button>
			</div>
		</div>
	</div>
</div>